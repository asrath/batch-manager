<?php
namespace Asrath\BatchManagerBundle\Manager;

use Asrath\BatchManagerBundle\Entity\Batch;

class BatchManager
{
    /**
     * @var Batch
     */
    private $batch;
    /**
     * @var int
     */
    private $total;


    /**
     * @param Batch $batch
     * @return $this
     * @throws \Exception
     */
    public function setBatch(Batch $batch)
    {
        if ($batch->getStatus() == Batch::STATUS_COMPLETE) {
            throw new \Exception('Batch already completed.');
        }
        $this->batch = $batch;
        $this->total = $this->batch->getTotal();

        return $this;
    }

    /**
     * @return Batch
     */
    public function getBatch()
    {
        return $this->batch;
    }

    /**
     * @return bool
     */
    public function hasBatch()
    {
        return !empty($this->batch);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function batchIsNew()
    {
        if ($this->hasBatch()) {
            $isNew = ($this->batch->getTotal() <= 0) &&
                $this->batch->getStatus() == Batch::STATUS_IN_PROGRESS;
        }
        else {
            throw new \Exception('No batch has been set');
        }

        return $isNew;
    }

    /**
     * @return string|null
     */
    public function getBatchOffset()
    {
        return $this->hasBatch() ? $this->batch->getOffset() : null;
    }

    /**
     * @param string|int $offset
     * @return $this
     * @throws \Exception
     */
    public function setInitialOffset($offset)
    {
        if ($this->batchIsNew()) {
            $this->batch->setOffset($offset);
        }
        else {
            throw new \Exception('This batch is not new. Offset can only be set in new batches');
        }

        return $this;
    }

    /**
     * @param int $limit
     *  0 for no limit
     * @return $this
     */
    public function setBatchLimit($limit)
    {
        if ($this->hasBatch()) {
            $this->batch->setLimit($limit);
        }

        return $this;
    }

    /**
     * @return int|null
     */
    public function getBatchLimit()
    {
        return $this->hasBatch() ? $this->batch->getLimit() : null;
    }

    /**
     * @return int|null
     */
    public function getTotal()
    {
        return $this->hasBatch() ? $this->total : null;
    }

    /**
     * @return int|null
     */
    public function getBatchTotal()
    {
        return $this->hasBatch() ? $this->batch->getTotal() : null;
    }

    /**
     * @return int|null
     */
    public function getProcessed()
    {
        return $this->hasBatch() ? $this->total - $this->batch->getTotal() : null;
    }

    /**
     * @param int $quantity [optional]
     * @return $this
     * @throws \Exception
     */
    public function batchIncTotal($quantity = 1)
    {
        if ($this->hasBatch()) {
            $this->total += $quantity;
            if ($this->getBatchLimit() > 0 && $this->total - $this->batch->getTotal() > $this->batch->getLimit()) {
                throw new \Exception('Batch limit exceeded.');
            }
            $this->batch->setUpdated(new \DateTime());
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function batchLimitReached()
    {
        return $this->hasBatch() ?
            $this->total - $this->batch->getTotal() >= $this->batch->getLimit() :
            $this->hasBatch();
    }

    /**
     * @param array $metadata
     * @return $this
     */
    public function setBatchMetadata(array $metadata)
    {
        $this->batch->setMetadata($metadata);

        return $this;
    }

    /**
     * @return array
     */
    public function getBatchMetadata()
    {
        return $this->batch->getMetadata();
    }

    public function markBatchComplete()
    {
        if ($this->hasBatch()) {
            $this->batch->setStatus(Batch::STATUS_COMPLETE);
        }
        else {
            throw new \Exception('No batch has been set');
        }
    }
}