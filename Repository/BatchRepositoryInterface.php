<?php
namespace Asrath\BatchManagerBundle\Repository;

interface BatchRepositoryInterface
{
    /**
     * @param string $code
     * @return \Asrath\BatchManagerBundle\Entity\Batch
     */
    public function getBatch($code);

    /**
     * @param string $code
     * @return null|\Asrath\BatchManagerBundle\Entity\Batch
     */
    public function getLastCompletedBatch($code);
}