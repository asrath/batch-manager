<?php

namespace Asrath\BatchManagerBundle\Repository;

use Asrath\BatchManagerBundle\Entity\Batch;

/**
 * BatchRepository
 *
 */
class BatchRepository extends \Doctrine\ORM\EntityRepository
implements BatchRepositoryInterface
{
    /**
     * @param string $code
     * @return Batch
     */
    public function getBatch($code)
    {
        $qb = $this->createQueryBuilder('b');
        $qb
            ->select('b')
            ->andWhere($qb->expr()->neq('b.status', ':status'))
            ->andWhere($qb->expr()->eq('b.code', ':code'))
            ->setParameter('status', Batch::STATUS_COMPLETE)
            ->setParameter('code', $code)
        ;
        $batch = $qb->getQuery()->getOneOrNullResult();

        if (empty($batch)) {
            $batch = new Batch();
            $batch->setCode($code);
        }

        return $batch;
    }

    /**
     * @param string $code
     * @return null|Batch
     */
    public function getLastCompletedBatch($code)
    {
        return $this->findOneBy(array(
            'code' => $code,
            'status' => Batch::STATUS_COMPLETE,
        ), array('updated' => 'DESC'));
    }
}
