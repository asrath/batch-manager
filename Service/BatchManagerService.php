<?php
namespace Asrath\BatchManagerBundle\Service;


use Asrath\BatchManagerBundle\Entity\Batch;
use Asrath\BatchManagerBundle\Manager\BatchManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class BatchManagerService
 * @package Asrath\BatchManagerBundle\Service
 *
 * @method void setBatch(Batch $batch)
 * @method Batch getBatch()
 * @method bool hasBatch()
 * @method bool batchIsNew()
 * @method bool batchLimitReached()
 * @method int|string getBatchOffset()
 * @method void setInitialOffset(int|string $offset)
 * @method void setBatchLimit(int $limit)
 * @method int getBatchLimit()
 * @method int|null getTotal()
 * @method int|null getBatchTotal()
 * @method int|null getProcessed()
 * @method void batchIncTotal(int $quantity)
 * @method void setBatchMetadata(array $metadata)
 * @method array getBatchMetadata()
 */
class BatchManagerService
{
    /**
     * @var EntityManager
     */
    protected $em;
    /**
     * @var BatchManager
     */
    protected $bm;

    public function __construct(RegistryInterface $doctrine, BatchManager $batchManager)
    {
        $this->em = $doctrine->getManager();
        $this->bm = $batchManager;
    }

    public function __call($name, $arguments)
    {
        if (method_exists($this->bm, $name)) {
            return call_user_func_array(array($this->bm, $name), $arguments);
        }
    }

    /**
     * @param string $code
     * @return Batch
     */
    public function retrieveBatch($code)
    {
        $batch = $this->em->getRepository('AsrathBatchManagerBundle:Batch')->getBatch($code);
        $this->bm->setBatch($batch);

        return $this->bm->getBatch();
    }

    /**
     * @param string $code
     * @return Batch|null
     */
    public function getLastCompletedBatch($code)
    {
        return $this->em->getRepository('AsrathBatchManagerBundle:Batch')->getLastCompletedBatch($code);
    }

    /**
     * @param string|int $offset [optional]
     * @return null|Batch
     */
    public function saveProgress($offset = NULL)
    {
        if ($this->hasBatch()) {
            if (!empty($offset)) {
                $this->getBatch()->setOffset($offset);
            }

            $this->getBatch()->setTotal($this->getTotal());
            $this->getBatch()->setUpdated(new \DateTime());
            $this->em->persist($this->getBatch());
            $this->em->flush($this->getBatch());
        }

        return $this->bm->getBatch();
    }

    public function markBatchComplete()
    {
        $this->bm->markBatchComplete();
        $this->saveProgress();
    }
}