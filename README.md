Asrath BatchManagerBundle
=========================

### Installation
Enable the bundle in `app/AppKernel.php`:
```php
<?php
// ...

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            // ...
            new Asrath\BatchManagerBundle\AsrathBatchManagerBundle(),
        ];
    }
}
```
Add the following to `app/config/routing.yml`
```yaml
asrath_batch_manager:
    resource: "@AsrathBatchManagerBundle/Resources/config/routing.yml"
    prefix:   /
```